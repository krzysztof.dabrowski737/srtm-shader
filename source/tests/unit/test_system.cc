/*******************************************************/
/**                                                   **/
/**   Author: Krzysztof Dabrowski                     **/
/**   e-mail: krzysztof.dabrowski737@gmail.com        **/
/**  Project: SRTM Shader                             **/
/**     year: 2023                                    **/
/**                                                   **/
/*******************************************************/

#include <gtest/gtest.h>

namespace srtm_shader {
namespace {

TEST(TESTING_ENVIRONMENT_WORKS, BASIC) {
    EXPECT_EQ(0, 0);
    EXPECT_FALSE(1 == 0);
    EXPECT_TRUE(1);
}

}
}