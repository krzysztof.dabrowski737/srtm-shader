/*******************************************************/
/**                                                   **/
/**   Author: Krzysztof Dabrowski                     **/
/**   e-mail: krzysztof.dabrowski737@gmail.com        **/
/**  Project: SRTM Shader                             **/
/**     year: 2023                                    **/
/**                                                   **/
/*******************************************************/

#ifndef SRTM_SHADER_IMAGE_VIEWER
#define SRTM_SHADER_IMAGE_VIEWER

#include "types/srtm_data.hpp"
#include <QImage>
#include <QMainWindow>

#include <srtm-shader/types/srtm_data.hpp>

namespace srtm_shader {

class ImageViewer {
public:
    ImageViewer();
    void show(const SrtmData & data);

private:
    QMainWindow window_;

};

}

#endif