/*******************************************************/
/**                                                   **/
/**   Author: Krzysztof Dabrowski                     **/
/**   e-mail: krzysztof.dabrowski737@gmail.com        **/
/**  Project: SRTM Shader                             **/
/**     year: 2023                                    **/
/**                                                   **/
/*******************************************************/

#ifndef SRTM_SHADER_SRTM_READER
#define SRTM_SHADER_SRTM_READER

#include <fstream>

#include <srtm-shader/types/srtm_data.hpp>

namespace srtm_shader {

class SrtmReader {
public:
    SrtmData read_hgt_file(std::ifstream & file);

private:
    uint32_t _get_file_size(std::ifstream & file);

};

}

#endif
