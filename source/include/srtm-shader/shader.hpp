/*******************************************************/
/**                                                   **/
/**   Author: Krzysztof Dabrowski                     **/
/**   e-mail: krzysztof.dabrowski737@gmail.com        **/
/**  Project: SRTM Shader                             **/
/**     year: 2023                                    **/
/**                                                   **/
/*******************************************************/

#ifndef SRTM_SHADER_SHADER
#define SRTM_SHADER_SHADER

#include "types/srtm_data.hpp"
#include <QImage>
#include <QMainWindow>

#include <srtm-shader/types/srtm_data.hpp>

namespace srtm_shader {

class Shader {
public:
    Shader();
    SrtmData shade(const SrtmData & data);

};

}

#endif