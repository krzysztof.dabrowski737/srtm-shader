/*******************************************************/
/**                                                   **/
/**   Author: Krzysztof Dabrowski                     **/
/**   e-mail: krzysztof.dabrowski737@gmail.com        **/
/**  Project: SRTM Shader                             **/
/**     year: 2023                                    **/
/**                                                   **/
/*******************************************************/

#ifndef SRTM_SHADER_SRTM_DATA
#define SRTM_SHADER_SRTM_DATA

#include <vector>
#include <cmath>
#include <cstdint>

namespace srtm_shader {

struct SrtmData {
    SrtmData() = delete;

    SrtmData(uint32_t size) : data(size)
    {
        rect_side_ = std::sqrt(data.size());
    }

    size_t rect_side() const { return rect_side_;}

    std::vector<uint16_t> data;

private:
    size_t rect_side_;
};

}

#endif