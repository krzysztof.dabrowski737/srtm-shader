/*******************************************************/
/**                                                   **/
/**   Author: Krzysztof Dabrowski                     **/
/**   e-mail: krzysztof.dabrowski737@gmail.com        **/
/**  Project: SRTM Shader                             **/
/**     year: 2023                                    **/
/**                                                   **/
/*******************************************************/

#include <iostream>

#include <QLabel>
#include <QPixmap>

#include <srtm-shader/image_viewer.hpp>

namespace srtm_shader {

ImageViewer::ImageViewer() {}

void ImageViewer::show(const SrtmData & data)
{  

    QImage img((const uchar *)data.data.data(), data.rect_side(), data.rect_side(), data.rect_side()*2, QImage::Format_Grayscale16);

    auto pix_map =  QPixmap::fromImage(img);
    //auto pix_map =  QPixmap::fromImage(img.scaled(1600,1000));

    QLabel *pix_label = new QLabel(&window_);
    pix_label->setPixmap(pix_map);
    pix_label->show();

    window_.setCentralWidget(pix_label);
    window_.show();    
}

}