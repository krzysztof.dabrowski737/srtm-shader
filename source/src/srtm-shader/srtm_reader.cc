/*******************************************************/
/**                                                   **/
/**   Author: Krzysztof Dabrowski                     **/
/**   e-mail: krzysztof.dabrowski737@gmail.com        **/
/**  Project: SRTM Shader                             **/
/**     year: 2023                                    **/
/**                                                   **/
/*******************************************************/

#include <iostream>
#include <algorithm>
#include <bit>
#include <cstdint>
#include <cstring>

#include <ostream>
#include <srtm-shader/srtm_reader.hpp>

namespace srtm_shader {

SrtmData SrtmReader::read_hgt_file(std::ifstream & file)
{
    auto file_size = _get_file_size(file);
    auto buffer = std::vector<int16_t>(file_size/2); // the data is in int16_t format (two bytes)

    file.read((char*) &buffer[0], file_size);

   auto format_data = [](int16_t & x){
        int16_t lsb = x << 8;
        x = ((uint16_t)x >> 8) | lsb;
       // x*=50; // TODO: Get rid of this
    };

    std::for_each(buffer.begin(), buffer.end(), format_data);

    auto data = srtm_shader::SrtmData(buffer.size());

    std::memcpy(&data.data[0], &buffer[0], file_size);

    return data;
}

 uint32_t SrtmReader::_get_file_size(std::ifstream & file)
 {
    auto fsize = file.tellg();
    file.seekg(0, std::ios::end);
    fsize = file.tellg() - fsize;

    file.seekg(0, std::ios::beg);

    return fsize;
 }

}