/*******************************************************/
/**                                                   **/
/**   Author: Krzysztof Dabrowski                     **/
/**   e-mail: krzysztof.dabrowski737@gmail.com        **/
/**  Project: SRTM Shader                             **/
/**     year: 2023                                    **/
/**                                                   **/
/*******************************************************/

#include <srtm-shader/shader.hpp>

namespace srtm_shader {

Shader::Shader(){}

SrtmData Shader::shade(const SrtmData & data)
{
    auto shaded = SrtmData(data.data.size());

    for(size_t i = 1; i < data.data.size(); i++)
    {
            shaded.data[i] = 20000 + 1000*(float(data.data[i]) - float(data.data[i-1]));
    }

    return shaded;
}

}