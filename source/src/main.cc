/*******************************************************/
/**                                                   **/
/**   Author: Krzysztof Dabrowski                     **/
/**   e-mail: krzysztof.dabrowski737@gmail.com        **/
/**  Project: SRTM Shader                             **/
/**     year: 2023                                    **/
/**                                                   **/
/*******************************************************/

#include <string>
#include <fstream>
#include <iostream>
#include <cmath>

#include <QApplication>

#include <srtm-shader/srtm_reader.hpp>
#include <srtm-shader/shader.hpp>
#include <srtm-shader/image_viewer.hpp>

std::string filename = "N49E022.hgt";

int main(int argc, char **argv) 
{
    QApplication app(argc, argv);

    std::ifstream hgt_file(filename, std::ios::binary);

    if(!hgt_file.is_open())
    {
        std::cout << "Could not open file " << filename << std::endl;
        return -1;
    }

    srtm_shader::SrtmReader reader;

    auto srtm_data = reader.read_hgt_file(hgt_file);

    srtm_shader::Shader shader;

    auto shaded_data = shader.shade(srtm_data);

    srtm_shader::ImageViewer viewer;

    viewer.show(shaded_data);

    return app.exec();
}