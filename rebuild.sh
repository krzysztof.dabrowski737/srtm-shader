#!/bin/sh
rm -rf build
mkdir build
cd build
cmake -DBUILD_TESTING=True -DQt5_DIR=/opt/Qt/5.15.2/gcc_64/lib/cmake/Qt5 ..
make -j24
